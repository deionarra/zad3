---
author: Aleksandra Ostrowska
title: Mars
subtitle: Czerwona Planeta
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---


## Nazewnictwo

**Mars** jest to czwarta od Słońca planeta Układu Słonecznego. Nazwana od imienia rzymskiego boga wojny – **Marsa**, zawdzięcza ją barwie, która przy obserwacji z Ziemi wydaje się \textcolor{red}{rdzawo-czerwona} i kojarzyła się starożytnym Rzymianom z pożogą wojenną. Odcień bierze się od tlenków żelaza pokrywających powierzchnię.

## Obraz Marsa

![](mars.jpg){ height=95% width=95%}

## Charakterystyka orbity (J2000)

| Lp. | Obszar             | Charakterystyka               |
| --- |:------------------:| -----------------------------:|
| 1.  | Ciało centralne    | Słońce                        | 
| 2.  | Perycentrum        | $2,0662\cdot10^{11}m$ 1,3814 au |
| 3.  | Apocentrum         | $2,4923\cdot10^{11m}$ 1,6660 au |
| 4.  | Okres orbitalny    | 686,980d                      |
| 5.  | Prędkość ruchu     | 21,97–26,50km/s               |

## Charakterystyka fizyczna

| Lp. | Obszar             | Charakterystyka                        |
| --- |:------------------:| --------------------------------------:|
| 1.  | Typ planety        | planeta skalista                       |
| 2.  | Masa               | $6,4171\cdot10^{23}kg$                   |
| 3.  | Pole powierzchni   | $1,448\cdot10^8km^2$                   |
| 4.  | Objętość           | $1,6318\cdot10^{11}km^3$                 |
| 5.  | Gęstość            | $3933kg/m^3$                           |
| 6.  | Okres obrotu       | 24,6229 h                              |
| 7.  | Temperatura        | $-140.15^{\circ}C$ – $19.85^{\circ}C$  |

## Charakterystyka atmosfery

1. Ciśnienie atmosferyczne - 400–870 Pa
2. Skład atmosfery
   * Dwutlenek węgla: 95,32\%
   * Azot: 2,7\%
   * Argon: 1,6\%
   * Tlen: 0,13\%
   * Tlenek węgla: 0,07\%
   * Pozostałe: para wodna, tlenek azotu, neon, HDO, krypton, ksenon

## Opis

Mars jest planetą wewnętrzną z cienką atmosferą, o powierzchni usianej kraterami uderzeniowymi, podobnie jak powierzchnia Księżyca i wielu innych ciał Układu Słonecznego. 

Występują na nim różne rodzaje terenu, podobne do ziemskich: wulkany, doliny, kaniony, pustynie i polarne czapy lodowe. 

Na Marsie znajduje się najwyższy wulkan w Układzie Słonecznym – Olympus Mons i największy kanion – Valles Marineris. 

W przeciwieństwie do Ziemi, Mars jest geologicznie i tektonicznie nieaktywny.

## Księżyce Marsa

Mars ma dwa małe księżyce o nieregularnych kształtach, których orbity są bardzo bliskie planety: **Fobosa** i **Deimosa**. Mogą być one przechwyconymi planetoidami lub ciałami utworzonymi z materii wyrzuconej przez uderzenia z powierzchni planety.

## Księżyce Marsa

![](moons.jpg){ height=95% width=95%}